package com.sony.nvx.prototype;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class MetadataExtractorServiceImplTest {
    
    private ImageMetadataService service;

    @Before
    public void setup() {
        service = new MetadataExtractorServiceImpl();
    }
    
    @Test
    public void testGetMetadata_XMPAndIPTC() {
        String name = "IPTCpanel.jpg";
        File file = new File(getClass().getClassLoader().getResource(name).getFile());
        Map<String, String> tagMap = service.getMetadata(file);
        for (String tagName : tagMap.keySet()) {
            System.out.println(String.format("%s = %s", tagName, tagMap.get(tagName)));
        }
        assertEquals(17, tagMap.size());
        assertTrue(tagMap.containsKey("Headline"));
    }    
}
