package com.sony.nvx.prototype;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ExiftoolServiceImplTest {
    
    private ImageMetadataService service;

    @Before
    public void setup() {
        service = new ExiftoolServiceImpl();
    }
    
    @Test
    public void testGetMetadata_IPTCTag_headline() {
        String name = "IPTCpanel.jpg";
        File file = new File(getClass().getClassLoader().getResource(name).getFile());
        Map<String, String> tagMap = service.getMetadata(file);
        for (String tagName : tagMap.keySet()) {
            System.out.println(String.format("%s = %s", tagName, tagMap.get(tagName)));
        }
        assertTrue(tagMap.containsKey("Headline"));
    }    
    
    @Test
    public void testGetMetadata_IPTCViaXMPTag_creator() {
        String name = "IPTCpanel.jpg";
        File file = new File(getClass().getClassLoader().getResource(name).getFile());
        Map<String, String> tagMap = service.getMetadata(file);
        for (String tagName : tagMap.keySet()) {
            System.out.println(String.format("%s = %s", tagName, tagMap.get(tagName)));
        }
        assertTrue(tagMap.containsKey("Creator"));
    }    
}
