package com.sony.nvx.prototype;

import com.thebuzzmedia.exiftool.Tag;

public enum IPTCTags implements Tag {

    HEADLINE("Headline", Type.STRING),
    CREATOR("Creator", Type.STRING);

    private final String name;

    private final Type type;

    private IPTCTags(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public <T> T parse(String value) {
        return type.parse(value);
    }

    @SuppressWarnings("unchecked")
    private static enum Type {
        INTEGER {
            @Override
            public <T> T parse(String value) {
                return (T) Integer.valueOf(Integer.parseInt(value));
            }
        },
        DOUBLE {
            @Override
            public <T> T parse(String value) {
                return (T) Double.valueOf(Double.parseDouble(value));
            }
        },
        STRING {
            @Override
            public <T> T parse(String value) {
                return (T) value;
            }
        };

        public abstract <T> T parse(String value);
    }
}