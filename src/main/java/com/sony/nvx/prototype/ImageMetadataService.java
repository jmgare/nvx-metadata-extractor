package com.sony.nvx.prototype;

import java.io.File;
import java.util.Map;

public interface ImageMetadataService {

    Map<String, String> getMetadata(File imageFile);
}
