package com.sony.nvx.prototype;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;

public class MetadataExtractorServiceImpl implements ImageMetadataService {
    
    private static final String IPTC = "IPTC";

    public Map<String, String> getMetadata(File imageFile) {
        Map<String, String> tagMap = new HashMap<>();
        Metadata metadata;
        try {
            metadata = ImageMetadataReader.readMetadata(imageFile);
            for (Directory directory : metadata.getDirectories()) {
                if (IPTC.equals(directory.getName())) {
                    for (Tag tag : directory.getTags()) {
                        tagMap.put(tag.getTagName(), tag.getDescription());
                    } 
                }
            }
        } catch (ImageProcessingException | IOException e) {
            e.printStackTrace();
        }
        return tagMap;        
    }    
}
