package com.sony.nvx.prototype;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.thebuzzmedia.exiftool.ExifTool;
import com.thebuzzmedia.exiftool.ExifToolBuilder;
import com.thebuzzmedia.exiftool.Tag;

public class ExiftoolServiceImpl implements ImageMetadataService {
    
    public Map<String, String> getMetadata(File imageFile) {
        Map<String, String> tagMap = new HashMap<>();
        try (ExifTool exifTool = new ExifToolBuilder().build()) {
            List<Tag> tags = new ArrayList<>();
            tags.add(IPTCTags.HEADLINE);
            tags.add(IPTCTags.CREATOR);
            Map<Tag, String> metadata = exifTool.getImageMeta(imageFile, tags);
            for (Tag tag : tags) {
                tagMap.put(tag.getName(), metadata.get(tag));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tagMap;
    }    
}
